require 'net/imap'
require 'net/http'
require "mail"
require "mini_exiftool"
require "RMagick"

def sanitize_filename(filename)
    filename.strip.tap do |name|
        # NOTE: File.basename doesn't work right with Windows paths on Unix
        # get only the filename, not the whole path
        name.sub! /\A.*(\\|\/)/, ''
        # Finally, replace all non alphanumeric, underscore
        # or periods with underscore
        name.gsub! /[^\w\.\-]/, '_'
    end
end

def image_processing(filename)
    # fix orientation
    new_img = Magick::Image.read("public/uploads/#{filename}").first
    new_img = new_img.auto_orient
    new_img.write("public/uploads/#{filename}")

    # create medium sized version
    medium_img = new_img.resize_to_fit(800)
    medium_img.write("public/uploads/medium_#{filename}")

    # create thumbnail
    thumb_img = new_img.resize_to_fill(100, 100)
    thumb_img.write("public/uploads/thumb_#{filename}")
end

namespace :get_photos do
    desc "downloads photos and adds them to database"
    task :getall => :environment do

        # avoid certificate issues
        OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE

        puts "[Starting: #{Time.now}]"
        # make a connection to imap account
        puts "connecting to mail server ... "


        puts "Server: #{PhotoShareConfig.config['imap_server']}:#{PhotoShareConfig.config['imap_port']}"
        puts "User: #{PhotoShareConfig.config['imap_user']}"
        puts "Folder: #{PhotoShareConfig.config['imap_folder']}"


        imap = Net::IMAP.new(PhotoShareConfig.config["imap_server"], PhotoShareConfig.config["imap_port"], true)
        imap.login(PhotoShareConfig.config["imap_user"], PhotoShareConfig.config["imap_password"])
        imap.select(PhotoShareConfig.config["imap_folder"])  # select mailbox to process

        uploadsList = Array.new
        if !File.directory?("public/uploads/") then Dir.mkdir("public/uploads/") end


        puts "retreiving list of emails..."
        # get all emails that are in the folder
        uid_list = imap.uid_search(["NOT", "NEW"])
        # get a list of all the UIDs we have retreived previously
        existing_uids = Array.new
        Upload.find_all_by_message_UID(uid_list).each do | existing_msg |
            existing_uids << existing_msg.message_UID
        end
        # remove the ones that we already have the the list
        uid_list = uid_list - existing_uids

        puts "retreiving emails ...."

        if uid_list.count == 0 then puts "No new emails to retreive." end 

        uid_list.each do | msg_uid |
            print "retreiving message: #{msg_uid}... "
            msg = Mail.new( imap.uid_fetch(msg_uid, 'RFC822').first.attr['RFC822'] )
            photosList = Array.new

            puts msg.subject

        # if not multipart and content type starts with "image/""
        if !(msg.multipart?) && msg.content_type.start_with?('image/') then
			begin
	        	#determine filename
	            fn = sanitize_filename("#{msg_uid}-#{msg.filename}")
	            # save decoded file
	            File.open( "public/uploads/#{fn}", "w+b", 0644 ) { |f| f.write msg.decoded }
	            image_processing(fn)

	            photodata = Hash.new
	            photodata["filename"] = fn
	            
	            exif_file = MiniExiftool.new "public/uploads/#{fn}"
	            photodata["CreateDate"] = exif_file['CreateDate']
	            photodata["GPS_ALT"] = exif_file['GPSAltitude']
	            photodata["GPS_LAT"] = exif_file['GPSLatitude']
	            photodata["GPS_LONG"] = exif_file['GPSLongitude']
	            photosList << photodata

	        rescue Exception => e
	        	puts "ERRRO: Unable to save data for #{fn} because #{e.message}"
			end
		end

        if (msg.multipart?) then 
            msg.attachments.each do |attachment|
                fn = "#{msg_uid}-#{attachment.filename}"
                fn = sanitize_filename(fn)
                # if attachment is an image
                if (attachment.content_type.start_with?('image/')) then
                    begin
                        puts "saving file #{fn}"
                        # save the attached file
                        File.open( "public/uploads/#{fn}", "w+b", 0644 ) { |f| f.write attachment.decoded }

	                image_processing(fn)

                        photodata = Hash.new
                        photodata["filename"] = fn
                        
                        exif_file = MiniExiftool.new "public/uploads/#{fn}"
                        photodata["CreateDate"] = exif_file['CreateDate']
                        photodata["GPS_ALT"] = exif_file['GPSAltitude']
                        photodata["GPS_LAT"] = exif_file['GPSLatitude']
                        photodata["GPS_LONG"] = exif_file['GPSLongitude']
                        photosList << photodata
                    rescue Exception => e
                        puts "ERRRO: Unable to save data for #{fn} because #{e.message}"
                    end
                end
            end
        end

            thisupload = Hash.new
            thisupload["message_UID"] = "#{msg_uid}"
            thisupload["sender"] = msg.header["from"]
            thisupload["subject"] = msg.subject
            thisupload["datesent"] = msg.date.to_s
            thisupload["photos"] = photosList

            puts "saving #{thisupload['message_UID']} to database"
            up = Upload.new
            up.message_UID = "#{thisupload['message_UID']}"
            up.datesent = "#{thisupload['datesent']}"
            up.sender = "#{thisupload['sender']}"
            up.subject = "#{thisupload['subject']}"
            up.save

            if photosList.length > 0 then

                thisupload['photos'].each do | photo |
                    puts "saving #{photo["filename"]} to database"
                    newphoto = Photo.new
                    newphoto.filename = photo["filename"]
                    newphoto.exif_createdate = photo["CreateDate"]
                    newphoto.exif_gpsaltitude = photo["GPS_ALT"]
                    newphoto.exif_gpslatitude = photo["GPS_LAT"]
                    newphoto.exif_gpslongitude = photo["GPS_LONG"]
                    newphoto.upload_id = up.id
                    newphoto.save
                end
            end
        end
        puts "[Done: #{Time.now}]"
    end
end
