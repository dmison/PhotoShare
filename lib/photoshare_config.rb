#lib/photoshare_config.rb

module PhotoShareConfig

  def self.config
    @@config ||= {}
  end

  def self.config=(hash)
    @@config = hash
  end

end
