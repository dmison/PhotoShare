class CreateUploads < ActiveRecord::Migration
  def change
    create_table :uploads do |t|
      t.integer :message_UID
      t.string :subject
      t.datetime :datesent
      t.string :sender

      t.timestamps
    end
  end
end
