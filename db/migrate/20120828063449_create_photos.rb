class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.string :filename
      t.datetime :exif_createdate
      t.string :exif_gpsaltitude
      t.string :exif_gpslatitude
      t.string :exif_gpslongitude

      t.timestamps
    end
  end
end
