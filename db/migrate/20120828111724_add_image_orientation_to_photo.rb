class AddImageOrientationToPhoto < ActiveRecord::Migration
  def change
    add_column :photos, :exif_orientation, :integer
  end
end
