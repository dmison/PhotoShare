class Upload < ActiveRecord::Base
  attr_accessible :datesent, :message_UID, :sender, :subject

  has_many :photos

  def teamname
    subject_p.strip!
    col_pos = subject_p.index(":")
    if col_pos == nil or col_pos == 0 then return " - no team - " end
    return subject_p[0 ... col_pos].strip
  end

  def photo_subject
    subject_p.strip!
    col_pos = subject_p.index(":")
    if col_pos == nil or col_pos == subject_p.length then return " - no subject - " end
    
    return subject_p[col_pos+1 ... subject_p.length].strip
  end


  def subject_p
     result = subject
     if (PhotoShareConfig.config['msg_prefix_pattern'] != "") then
         pattern = Regexp.new(PhotoShareConfig.config['msg_prefix_pattern'])
         result = subject.gsub(pattern, "")
     end
     return result.strip
   end

   def sender_name
      sender.strip!
      first_angle = sender.index("<")
      if (first_angle == nil || first_angle == 0) then return self.sender_email end
      return sender[0, first_angle].strip;
   end
   
   def sender_email
      sender.strip!
      first_angle = sender.index("<")
      second_angle = sender.index(">")
      if first_angle == nil then return sender end
      return sender[(first_angle+1) ... (second_angle)].strip
   end

end
