class Photo < ActiveRecord::Base
  attr_accessible :exif_createdate, :exif_gpsaltitude, :exif_gpslatitude, :exif_gpslongitude, :filename

  belongs_to :upload

  def timestamp
  	
  	if exif_createdate != nil then
  		return exif_createdate
  	else
  		return upload.datesent
  	end

  end

end
